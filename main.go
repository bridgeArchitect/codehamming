package main

import "fmt"

/* structure to save bits of message */
type Message struct {
	bits [9]byte
}

/* entry point */
func main() {

	/* declaration of variables */
	var (
		rows  [10]string
		mess  [10]Message
		i     int
		corr  [4]byte
		c0    byte
		c1    byte
		c2    byte
		c3    byte
		c4    byte
	)

	/* initial symbols */
	rows[0] = "0ED"
	rows[1] = "0C5"
	rows[2] = "0F6"
	rows[3] = "1BF"
	rows[4] = "115"
	rows[5] = "1D4"
	rows[6] = "169"
	rows[7] = "1F0"
	rows[8] = "1EC"
	rows[9] = "1CD"

	/* go through all messages */
	for i = 0; i < len(rows); i++ {

		/* decode the first bit (from hex code) */
		if rows[i][0] == '0' {
			mess[i].bits[0] = 0
		} else if rows[i][0] == '1' {
			mess[i].bits[0] = 1
		}

		/* decode tha next four bits (from hex code) */
		if rows[i][1] == '0' {
			mess[i].bits[1] = 0
			mess[i].bits[2] = 0
			mess[i].bits[3] = 0
			mess[i].bits[4] = 0
		} else if rows[i][1] == '1' {
			mess[i].bits[1] = 0
			mess[i].bits[2] = 0
			mess[i].bits[3] = 0
			mess[i].bits[4] = 1
		} else if rows[i][1] == '2' {
			mess[i].bits[1] = 0
			mess[i].bits[2] = 0
			mess[i].bits[3] = 1
			mess[i].bits[4] = 0
		} else if rows[i][1] == '3' {
			mess[i].bits[1] = 0
			mess[i].bits[2] = 0
			mess[i].bits[3] = 1
			mess[i].bits[4] = 1
		} else if rows[i][1] == '4' {
			mess[i].bits[1] = 0
			mess[i].bits[2] = 1
			mess[i].bits[3] = 0
			mess[i].bits[4] = 0
		} else if rows[i][1] == '5' {
			mess[i].bits[1] = 0
			mess[i].bits[2] = 1
			mess[i].bits[3] = 0
			mess[i].bits[4] = 1
		} else if rows[i][1] == '6' {
			mess[i].bits[1] = 0
			mess[i].bits[2] = 1
			mess[i].bits[3] = 1
			mess[i].bits[4] = 0
		} else if rows[i][1] == '7' {
			mess[i].bits[1] = 0
			mess[i].bits[2] = 1
			mess[i].bits[3] = 1
			mess[i].bits[4] = 1
		} else if rows[i][1] == '8' {
			mess[i].bits[1] = 1
			mess[i].bits[2] = 0
			mess[i].bits[3] = 0
			mess[i].bits[4] = 0
		} else if rows[i][1] == '9' {
			mess[i].bits[1] = 1
			mess[i].bits[2] = 0
			mess[i].bits[3] = 0
			mess[i].bits[4] = 1
		} else if rows[i][1] == 'A' {
			mess[i].bits[1] = 1
			mess[i].bits[2] = 0
			mess[i].bits[3] = 1
			mess[i].bits[4] = 0
		} else if rows[i][1] == 'B' {
			mess[i].bits[1] = 1
			mess[i].bits[2] = 0
			mess[i].bits[3] = 1
			mess[i].bits[4] = 1
		} else if rows[i][1] == 'C' {
			mess[i].bits[1] = 1
			mess[i].bits[2] = 1
			mess[i].bits[3] = 0
			mess[i].bits[4] = 0
		} else if rows[i][1] == 'D' {
			mess[i].bits[1] = 1
			mess[i].bits[2] = 1
			mess[i].bits[3] = 0
			mess[i].bits[4] = 1
		} else if rows[i][1] == 'E' {
			mess[i].bits[1] = 1
			mess[i].bits[2] = 1
			mess[i].bits[3] = 1
			mess[i].bits[4] = 0
		} else if rows[i][1] == 'F' {
			mess[i].bits[1] = 1
			mess[i].bits[2] = 1
			mess[i].bits[3] = 1
			mess[i].bits[4] = 1
		}

		/* decode tha next four bits (from hex code) */
		if rows[i][2] == '0' {
			mess[i].bits[5] = 0
			mess[i].bits[6] = 0
			mess[i].bits[7] = 0
			mess[i].bits[8] = 0
		} else if rows[i][2] == '1' {
			mess[i].bits[5] = 0
			mess[i].bits[6] = 0
			mess[i].bits[7] = 0
			mess[i].bits[8] = 1
		} else if rows[i][2] == '2' {
			mess[i].bits[5] = 0
			mess[i].bits[6] = 0
			mess[i].bits[7] = 1
			mess[i].bits[8] = 0
		} else if rows[i][2] == '3' {
			mess[i].bits[5] = 0
			mess[i].bits[6] = 0
			mess[i].bits[7] = 1
			mess[i].bits[8] = 1
		} else if rows[i][2] == '4' {
			mess[i].bits[5] = 0
			mess[i].bits[6] = 1
			mess[i].bits[7] = 0
			mess[i].bits[8] = 0
		} else if rows[i][2] == '5' {
			mess[i].bits[5] = 0
			mess[i].bits[6] = 1
			mess[i].bits[7] = 0
			mess[i].bits[8] = 1
		} else if rows[i][2] == '6' {
			mess[i].bits[5] = 0
			mess[i].bits[6] = 1
			mess[i].bits[7] = 1
			mess[i].bits[8] = 0
		} else if rows[i][2] == '7' {
			mess[i].bits[5] = 0
			mess[i].bits[6] = 1
			mess[i].bits[7] = 1
			mess[i].bits[8] = 1
		} else if rows[i][2] == '8' {
			mess[i].bits[5] = 1
			mess[i].bits[6] = 0
			mess[i].bits[7] = 0
			mess[i].bits[8] = 0
		} else if rows[i][2] == '9' {
			mess[i].bits[5] = 1
			mess[i].bits[6] = 0
			mess[i].bits[7] = 0
			mess[i].bits[8] = 1
		} else if rows[i][2] == 'A' {
			mess[i].bits[5] = 1
			mess[i].bits[6] = 0
			mess[i].bits[7] = 1
			mess[i].bits[8] = 0
		} else if rows[i][2] == 'B' {
			mess[i].bits[5] = 1
			mess[i].bits[6] = 0
			mess[i].bits[7] = 1
			mess[i].bits[8] = 1
		} else if rows[i][2] == 'C' {
			mess[i].bits[5] = 1
			mess[i].bits[6] = 1
			mess[i].bits[7] = 0
			mess[i].bits[8] = 0
		} else if rows[i][2] == 'D' {
			mess[i].bits[5] = 1
			mess[i].bits[6] = 1
			mess[i].bits[7] = 0
			mess[i].bits[8] = 1
		} else if rows[i][2] == 'E' {
			mess[i].bits[5] = 1
			mess[i].bits[6] = 1
			mess[i].bits[7] = 1
			mess[i].bits[8] = 0
		} else if rows[i][2] == 'F' {
			mess[i].bits[5] = 1
			mess[i].bits[6] = 1
			mess[i].bits[7] = 1
			mess[i].bits[8] = 1
		}

	}

	/* go through all symbols */
	for i = 0; i < len(mess); i++ {

		/* find numbers for correcting */
		corr[0] = mess[i].bits[5] ^ mess[i].bits[0] ^ mess[i].bits[8] ^ mess[i].bits[3] ^ mess[i].bits[1]
		corr[1] = mess[i].bits[2] ^ mess[i].bits[0] ^ mess[i].bits[4] ^ mess[i].bits[3]
		corr[2] = mess[i].bits[7] ^ mess[i].bits[8] ^ mess[i].bits[4] ^ mess[i].bits[3]
		corr[3] = mess[i].bits[6] ^ mess[i].bits[1]

		/* apply correction */
		if (corr[0] == 0) && (corr[1] == 1) && (corr[2] == 1) && (corr[3] == 0) {
			mess[i].bits[4] = (mess[i].bits[4] + 1) % 2
		}
		if (corr[0] == 1) && (corr[1] == 0) && (corr[2] == 0) && (corr[3] == 1) {
			mess[i].bits[1] = (mess[i].bits[1] + 1) % 2
		}
		if (corr[0] == 1) && (corr[1] == 1) && (corr[2] == 0) && (corr[3] == 0) {
			mess[i].bits[0] = (mess[i].bits[0] + 1) % 2
		}
		if (corr[0] == 1) && (corr[1] == 0) && (corr[2] == 1) && (corr[3] == 0) {
			mess[i].bits[8] = (mess[i].bits[8] + 1) % 2
		}
		if (corr[0] == 1) && (corr[1] == 1) && (corr[2] == 1) && (corr[3] == 0) {
			mess[i].bits[3] = (mess[i].bits[3] + 1) % 2
		}
		if (corr[0] == 0) && (corr[1] == 1) && (corr[2] == 0) && (corr[3] == 0) {
			mess[i].bits[2] = (mess[i].bits[2] + 1) % 2
		}
		if (corr[0] == 1) && (corr[1] == 0) && (corr[2] == 0) && (corr[3] == 0) {
			mess[i].bits[5] = (mess[i].bits[5] + 1) % 2
		}
		if (corr[0] == 0) && (corr[1] == 0) && (corr[2] == 0) && (corr[3] == 1) {
			mess[i].bits[6] = (mess[i].bits[6] + 1) % 2
		}
		if (corr[0] == 0) && (corr[1] == 0) && (corr[2] == 1) && (corr[3] == 0) {
			mess[i].bits[7] = (mess[i].bits[7] + 1) % 2
		}

	}

	/* go through all symbols */
	for i = 0; i < len(mess); i++ {

		/* write the first corrected symbol */
		if mess[i].bits[0] == 0 {
			fmt.Print("0")
		} else if mess[i].bits[0] == 1 {
			fmt.Print("1")
		}

		/* write the second corrected symbol */
		if (mess[i].bits[1] == 0) && (mess[i].bits[2] == 0) && (mess[i].bits[3] == 0) && (mess[i].bits[4] == 0) {
			fmt.Print("0")
		} else if (mess[i].bits[1] == 0) && (mess[i].bits[2] == 0) && (mess[i].bits[3] == 0) && (mess[i].bits[4] == 1) {
			fmt.Print("1")
		} else if (mess[i].bits[1] == 0) && (mess[i].bits[2] == 0) && (mess[i].bits[3] == 1) && (mess[i].bits[4] == 0) {
			fmt.Print("2")
		} else if (mess[i].bits[1] == 0) && (mess[i].bits[2] == 0) && (mess[i].bits[3] == 1) && (mess[i].bits[4] == 1) {
			fmt.Print("3")
		} else if (mess[i].bits[1] == 0) && (mess[i].bits[2] == 1) && (mess[i].bits[3] == 0) && (mess[i].bits[4] == 0) {
			fmt.Print("4")
		} else if (mess[i].bits[1] == 0) && (mess[i].bits[2] == 1) && (mess[i].bits[3] == 0) && (mess[i].bits[4] == 1) {
			fmt.Print("5")
		} else if (mess[i].bits[1] == 0) && (mess[i].bits[2] == 1) && (mess[i].bits[3] == 1) && (mess[i].bits[4] == 0) {
			fmt.Print("6")
		} else if (mess[i].bits[1] == 0) && (mess[i].bits[2] == 1) && (mess[i].bits[3] == 1) && (mess[i].bits[4] == 1) {
			fmt.Print("7")
		} else if (mess[i].bits[1] == 1) && (mess[i].bits[2] == 0) && (mess[i].bits[3] == 0) && (mess[i].bits[4] == 0) {
			fmt.Print("8")
		} else if (mess[i].bits[1] == 1) && (mess[i].bits[2] == 0) && (mess[i].bits[3] == 0) && (mess[i].bits[4] == 1) {
			fmt.Print("9")
		} else if (mess[i].bits[1] == 1) && (mess[i].bits[2] == 0) && (mess[i].bits[3] == 1) && (mess[i].bits[4] == 0) {
			fmt.Print("A")
		} else if (mess[i].bits[1] == 1) && (mess[i].bits[2] == 0) && (mess[i].bits[3] == 1) && (mess[i].bits[4] == 1) {
			fmt.Print("B")
		} else if (mess[i].bits[1] == 1) && (mess[i].bits[2] == 1) && (mess[i].bits[3] == 0) && (mess[i].bits[4] == 0) {
			fmt.Print("C")
		} else if (mess[i].bits[1] == 1) && (mess[i].bits[2] == 1) && (mess[i].bits[3] == 0) && (mess[i].bits[4] == 1) {
			fmt.Print("D")
		} else if (mess[i].bits[1] == 1) && (mess[i].bits[2] == 1) && (mess[i].bits[3] == 1) && (mess[i].bits[4] == 0) {
			fmt.Print("E")
		} else if (mess[i].bits[1] == 1) && (mess[i].bits[2] == 1) && (mess[i].bits[3] == 1) && (mess[i].bits[4] == 1) {
			fmt.Print("F")
		}

		/* write the third corrected symbol */
		if (mess[i].bits[5] == 0) && (mess[i].bits[6] == 0) && (mess[i].bits[7] == 0) && (mess[i].bits[8] == 0) {
			fmt.Print("0")
		} else if (mess[i].bits[5] == 0) && (mess[i].bits[6] == 0) && (mess[i].bits[7] == 0) && (mess[i].bits[8] == 1) {
			fmt.Print("1")
		} else if (mess[i].bits[5] == 0) && (mess[i].bits[6] == 0) && (mess[i].bits[7] == 1) && (mess[i].bits[8] == 0) {
			fmt.Print("2")
		} else if (mess[i].bits[5] == 0) && (mess[i].bits[6] == 0) && (mess[i].bits[7] == 1) && (mess[i].bits[8] == 1) {
			fmt.Print("3")
		} else if (mess[i].bits[5] == 0) && (mess[i].bits[6] == 1) && (mess[i].bits[7] == 0) && (mess[i].bits[8] == 0) {
			fmt.Print("4")
		} else if (mess[i].bits[5] == 0) && (mess[i].bits[6] == 1) && (mess[i].bits[7] == 0) && (mess[i].bits[8] == 1) {
			fmt.Print("5")
		} else if (mess[i].bits[5] == 0) && (mess[i].bits[6] == 1) && (mess[i].bits[7] == 1) && (mess[i].bits[8] == 0) {
			fmt.Print("6")
		} else if (mess[i].bits[5] == 0) && (mess[i].bits[6] == 1) && (mess[i].bits[7] == 1) && (mess[i].bits[8] == 1) {
			fmt.Print("7")
		} else if (mess[i].bits[5] == 1) && (mess[i].bits[6] == 0) && (mess[i].bits[7] == 0) && (mess[i].bits[8] == 0) {
			fmt.Print("8")
		} else if (mess[i].bits[5] == 1) && (mess[i].bits[6] == 0) && (mess[i].bits[7] == 0) && (mess[i].bits[8] == 1) {
			fmt.Print("9")
		} else if (mess[i].bits[5] == 1) && (mess[i].bits[6] == 0) && (mess[i].bits[7] == 1) && (mess[i].bits[8] == 0) {
			fmt.Print("A")
		} else if (mess[i].bits[5] == 1) && (mess[i].bits[6] == 0) && (mess[i].bits[7] == 1) && (mess[i].bits[8] == 1) {
			fmt.Print("B")
		} else if (mess[i].bits[5] == 1) && (mess[i].bits[6] == 1) && (mess[i].bits[7] == 0) && (mess[i].bits[8] == 0) {
			fmt.Print("C")
		} else if (mess[i].bits[5] == 1) && (mess[i].bits[6] == 1) && (mess[i].bits[7] == 0) && (mess[i].bits[8] == 1) {
			fmt.Print("D")
		} else if (mess[i].bits[5] == 1) && (mess[i].bits[6] == 1) && (mess[i].bits[7] == 1) && (mess[i].bits[8] == 0) {
			fmt.Print("E")
		} else if (mess[i].bits[5] == 1) && (mess[i].bits[6] == 1) && (mess[i].bits[7] == 1) && (mess[i].bits[8] == 1) {
			fmt.Print("F")
		}

		/* find out information bits */
		c0 = mess[i].bits[0]
		c1 = mess[i].bits[8]
		c2 = mess[i].bits[4]
		c3 = mess[i].bits[3]
		c4 = mess[i].bits[1]

		/* write the first symbol to specify final result */
		fmt.Print(" ")
		if c0 == 0 {
			fmt.Print("0")
		} else if c0 == 1 {
			fmt.Print("1")
		}

		/* write the second symbol to specify final result */
		if (c1 == 0) && (c2 == 0) && (c3 == 0) && (c4 == 0) {
			fmt.Print("0")
		} else if (c1 == 0) && (c2 == 0) && (c3 == 0) && (c4 == 1) {
			fmt.Print("1")
		} else if (c1 == 0) && (c2 == 0) && (c3 == 1) && (c4 == 0) {
			fmt.Print("2")
		} else if (c1 == 0) && (c2 == 0) && (c3 == 1) && (c4 == 1) {
			fmt.Print("3")
		} else if (c1 == 0) && (c2 == 1) && (c3 == 0) && (c4 == 0) {
			fmt.Print("4")
		} else if (c1 == 0) && (c2 == 1) && (c3 == 0) && (c4 == 1) {
			fmt.Print("5")
		} else if (c1 == 0) && (c2 == 1) && (c3 == 1) && (c4 == 0) {
			fmt.Print("6")
		} else if (c1 == 0) && (c2 == 1) && (c3 == 1) && (c4 == 1) {
			fmt.Print("7")
		} else if (c1 == 1) && (c2 == 0) && (c3 == 0) && (c4 == 0) {
			fmt.Print("8")
		} else if (c1 == 1) && (c2 == 0) && (c3 == 0) && (c4 == 1) {
			fmt.Print("9")
		} else if (c1 == 1) && (c2 == 0) && (c3 == 1) && (c4 == 0) {
			fmt.Print("A")
		} else if (c1 == 1) && (c2 == 0) && (c3 == 1) && (c4 == 1) {
			fmt.Print("B")
		} else if (c1 == 1) && (c2 == 1) && (c3 == 0) && (c4 == 0) {
			fmt.Print("C")
		} else if (c1 == 1) && (c2 == 1) && (c3 == 0) && (c4 == 1) {
			fmt.Print("D")
		} else if (c1 == 1) && (c2 == 1) && (c3 == 1) && (c4 == 0) {
			fmt.Print("E")
		} else if (c1 == 1) && (c2 == 1) && (c3 == 1) && (c4 == 1) {
			fmt.Print("F")
		}
		fmt.Println()

	}

}